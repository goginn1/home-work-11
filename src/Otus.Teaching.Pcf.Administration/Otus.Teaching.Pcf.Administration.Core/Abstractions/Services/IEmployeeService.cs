﻿
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}
