﻿

using RabbitMQ.Client;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.Integration.Options;
using RabbitMQ.Client.Events;
using System.Text;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using System;
using Microsoft.Extensions.Hosting;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;

namespace Otus.Teaching.Pcf.Administration.Integration
{
    public class ConsumerAdministrator : BackgroundService
    {
        private readonly string _exchange = "Administrator.Exchange";
        private readonly string _queue = "Administration.Queue";

        private readonly IServiceScopeFactory _serivceFactory;

        private readonly string[] _routingKey = new string[]
        {
            "Administrator.PromoCode"
        };

        private readonly ConnectionFactory _connectionFactory;

        public ConsumerAdministrator(IOptions<RabbitSettings> rabbitSettings, IServiceScopeFactory serivceFactory)
        {
            _serivceFactory = serivceFactory;

            _connectionFactory = new ConnectionFactory()
            {
                UserName = rabbitSettings.Value.User,
                Password = rabbitSettings.Value.Password,
                Port = 5672,
                HostName = rabbitSettings.Value.Server,
                VirtualHost = rabbitSettings.Value.VirtualHost
            };
        }

  
        private IModel InitConsmer()
        {
            var con = _connectionFactory.CreateConnection();

            var channel = con.CreateModel();

            channel.QueueDeclare(
                queue: _queue,
                exclusive: false,
                durable: true);

            foreach (var key in _routingKey)
            {
                channel.QueueBind(
                    queue: _queue,
                    exchange: _exchange,
                    routingKey: key);
            }

            return channel;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var _channel = InitConsmer();
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (s, data) =>
            {
                var str = Encoding.UTF8.GetString(data.Body.ToArray());

                var guidId = JsonSerializer.Deserialize<Guid>(str);

                UpdateApplied(guidId);

                _channel.BasicAck(data.DeliveryTag, false);
            };
            _channel.BasicConsume(queue: _queue, autoAck: false, consumer: consumer);

            return Task.CompletedTask;
        }

        private void UpdateApplied(Guid guidId)
        {
            using (var scope = _serivceFactory.CreateScope())
            {
                var employeeService = scope.ServiceProvider.GetService<IEmployeeService>();

                employeeService.UpdateAppliedPromocodesAsync(guidId);
            }
        }
    }
}
