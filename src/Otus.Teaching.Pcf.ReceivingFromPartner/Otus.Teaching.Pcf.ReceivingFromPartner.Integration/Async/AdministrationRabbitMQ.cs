﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Async
{
    public class AdministrationRabbitMQ
        : IAdministrationGateway
    {
        private readonly string _exchange = "Administrator.Exchange";


        private readonly ConnectionFactory _connectionFactory;

        public AdministrationRabbitMQ(IOptions<RabbitSettings> rabbitSettings)
        {
            _connectionFactory = new ConnectionFactory()
            {
                UserName = rabbitSettings.Value.User,
                Password = rabbitSettings.Value.Password,
                Port = 5672,
                HostName = rabbitSettings.Value.Server,
                VirtualHost = rabbitSettings.Value.VirtualHost
            };

            using var con = _connectionFactory.CreateConnection();

            if (con.IsOpen)
            {
                using var channel = con.CreateModel();

                channel.ExchangeDeclare(
                    exchange: _exchange,
                    type: ExchangeType.Direct,
                    durable: true);
            }
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            using var con = _connectionFactory.CreateConnection();

            if (con.IsOpen)
            {
                using var channel = con.CreateModel();

                var body = JsonSerializer.Serialize(partnerManagerId);
                var bytes = Encoding.UTF8.GetBytes(body);

                channel.BasicPublish(
                    exchange: _exchange,
                    routingKey: "Administrator.PromoCode",
                    body: bytes);
            }
        }
    }
}